import sys
import pika
import time
import random
import json

connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost', port=5672))
channel = connection.channel()
channel.queue_declare(queue='queue_task', durable=True)
message = ' '.join(sys.argv[1:]) or "Hello World!"
num = 1
while True:
    
    point = "." * random.randint(1,10)
    
    message = "message {}".format(point)
    dict_mess = {"mess": message,
                 "num": num,}
    
    channel.basic_publish(exchange='',
                          routing_key='queue_task',
                          body=json.dumps(dict_mess),
                          properties=pika.BasicProperties(
                          delivery_mode = 2, # make message persistent
                          ))
    print (" [x] Sent %r %r" % (str(dict_mess["num"]), dict_mess["mess"]))
    num += 1
    time.sleep(2)
