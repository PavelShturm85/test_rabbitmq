import time
import json
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='task_queue', durable=True)

print (' [*] Waiting for messages. To exit press CTRL+C')

def callback(ch, method, properties, body):
    body = json.loads(body)

    print (" [x] Received %r %r" % (body["num"], body["mess"]))
    time.sleep(body["mess"].count('.'))
    print (" [x] Done")
    ch.basic_ack(delivery_tag = method.delivery_tag)

channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback,
                      queue='queue_task')

channel.start_consuming()